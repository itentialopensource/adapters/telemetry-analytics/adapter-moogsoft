
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_21:12PM

See merge request itentialopensource/adapters/adapter-moogsoft!13

---

## 0.4.3 [09-01-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-moogsoft!11

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_19:26PM

See merge request itentialopensource/adapters/adapter-moogsoft!10

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_21:08PM

See merge request itentialopensource/adapters/adapter-moogsoft!9

---

## 0.4.0 [07-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-moogsoft!8

---

## 0.3.3 [03-27-2024]

* Changes made at 2024.03.27_13:21PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-moogsoft!7

---

## 0.3.2 [03-12-2024]

* Changes made at 2024.03.12_11:02AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-moogsoft!6

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:38AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-moogsoft!5

---

## 0.3.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-moogsoft!4

---
