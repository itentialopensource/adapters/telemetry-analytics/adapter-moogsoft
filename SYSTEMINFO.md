# Moogsoft

Vendor: Moogsoft
Homepage: https://www.moogsoft.com/

Product: Moogsoft
Product Page: https://www.moogsoft.com/

## Introduction
We classify Moogsoft into the Service Assurance domain as Moogsoft is an operations management platform that enables proactive incident detection, root cause analysis, and resolution through automated monitoring and alerting.

## Why Integrate
The Moogsoft adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Moogsoft. With this adapter you have the ability to perform operations on items such as:

- Alerts
- Situations
- Integrations

## Additional Product Documentation
The [API documents for Moogsoft](https://docs.moogsoft.com/Enterprise.8.0.0/en/graze-api-endpoint-reference.html)
