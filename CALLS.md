## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Moogsoft. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Moogsoft.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Moogsoft. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">addAlertCustomInfo(body, callback)</td>
    <td style="padding:15px">addAlertCustomInfo</td>
    <td style="padding:15px">{base_path}/{version}/addAlertCustomInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignAlert(body, callback)</td>
    <td style="padding:15px">assignAlert</td>
    <td style="padding:15px">{base_path}/{version}/assignAlert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignAndAcknowledgeAlert(body, callback)</td>
    <td style="padding:15px">assignAndAcknowledgeAlert</td>
    <td style="padding:15px">{base_path}/{version}/assignAndAcknowledgeAlert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">closeAlert(body, callback)</td>
    <td style="padding:15px">closeAlert</td>
    <td style="padding:15px">{base_path}/{version}/closeAlert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deassignAlert(body, callback)</td>
    <td style="padding:15px">deassignAlert</td>
    <td style="padding:15px">{base_path}/{version}/deassignAlert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertDetails(body, callback)</td>
    <td style="padding:15px">getAlertDetails</td>
    <td style="padding:15px">{base_path}/{version}/getAlertDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertActions(body, callback)</td>
    <td style="padding:15px">getAlertActions</td>
    <td style="padding:15px">{base_path}/{version}/getAlertActions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertIds(body, callback)</td>
    <td style="padding:15px">getAlertIds</td>
    <td style="padding:15px">{base_path}/{version}/getAlertIds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrcLabels(sitnId, alertIds, callback)</td>
    <td style="padding:15px">getPrcLabels</td>
    <td style="padding:15px">{base_path}/{version}/getPrcLabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSeverities(callback)</td>
    <td style="padding:15px">getSeverities</td>
    <td style="padding:15px">{base_path}/{version}/getSeverities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatuses(callback)</td>
    <td style="padding:15px">getStatuses</td>
    <td style="padding:15px">{base_path}/{version}/getStatuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopPrcDetails(body, callback)</td>
    <td style="padding:15px">getTopPrcDetails</td>
    <td style="padding:15px">{base_path}/{version}/getTopPrcDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resolveAlerts(body, callback)</td>
    <td style="padding:15px">resolveAlerts</td>
    <td style="padding:15px">{base_path}/{version}/resolveAlerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setAlertAcknowledgeState(body, callback)</td>
    <td style="padding:15px">setAlertAcknowledgeState</td>
    <td style="padding:15px">{base_path}/{version}/setAlertAcknowledgeState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setAlertSeverity(body, callback)</td>
    <td style="padding:15px">setAlertSeverity</td>
    <td style="padding:15px">{base_path}/{version}/setAlertSeverity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setPrcLabels(body, callback)</td>
    <td style="padding:15px">setPrcLabels</td>
    <td style="padding:15px">{base_path}/{version}/setPrcLabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertsInNewSituationsStats(body, callback)</td>
    <td style="padding:15px">getAlertsInNewSituationsStats</td>
    <td style="padding:15px">{base_path}/{version}/getAlertsInNewSituationsStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertsMarkedPRCPerUserStats(body, callback)</td>
    <td style="padding:15px">getAlertsMarkedPRCPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getAlertsMarkedPRCPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNewAlertsStats(body, callback)</td>
    <td style="padding:15px">getNewAlertsStats</td>
    <td style="padding:15px">{base_path}/{version}/getNewAlertsStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNewEventsPerAlertsStats(body, callback)</td>
    <td style="padding:15px">getNewEventsPerAlertsStats</td>
    <td style="padding:15px">{base_path}/{version}/getNewEventsPerAlertsStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAlertToSituation(body, callback)</td>
    <td style="padding:15px">addAlertToSituation</td>
    <td style="padding:15px">{base_path}/{version}/addAlertToSituation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSigCorrelationInfo(body, callback)</td>
    <td style="padding:15px">addSigCorrelationInfo</td>
    <td style="padding:15px">{base_path}/{version}/addSigCorrelationInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSituationCustomInfo(body, callback)</td>
    <td style="padding:15px">addSituationCustomInfo</td>
    <td style="padding:15px">{base_path}/{version}/addSituationCustomInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignAndAcknowledgeSituation(body, callback)</td>
    <td style="padding:15px">assignAndAcknowledgeSituation</td>
    <td style="padding:15px">{base_path}/{version}/assignAndAcknowledgeSituation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignSituation(body, callback)</td>
    <td style="padding:15px">assignSituation</td>
    <td style="padding:15px">{base_path}/{version}/assignSituation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignTeamsToSituation(body, callback)</td>
    <td style="padding:15px">assignTeamsToSituation</td>
    <td style="padding:15px">{base_path}/{version}/assignTeamsToSituation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkSituationFlag(sitnId, flag, callback)</td>
    <td style="padding:15px">checkSituationFlag</td>
    <td style="padding:15px">{base_path}/{version}/checkSituationFlag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">closeSituation(body, callback)</td>
    <td style="padding:15px">closeSituation</td>
    <td style="padding:15px">{base_path}/{version}/closeSituation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSituation(body, callback)</td>
    <td style="padding:15px">createSituation</td>
    <td style="padding:15px">{base_path}/{version}/createSituation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deassignSituation(body, callback)</td>
    <td style="padding:15px">deassignSituation</td>
    <td style="padding:15px">{base_path}/{version}/deassignSituation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveSituationIds(callback)</td>
    <td style="padding:15px">getActiveSituationIds</td>
    <td style="padding:15px">{base_path}/{version}/getActiveSituationIds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSigCorrelationInfo(sitnId, callback)</td>
    <td style="padding:15px">getSigCorrelationInfo</td>
    <td style="padding:15px">{base_path}/{version}/getSigCorrelationInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSimilarSituationIds(body, callback)</td>
    <td style="padding:15px">getSimilarSituationIds</td>
    <td style="padding:15px">{base_path}/{version}/getSimilarSituationIds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSimilarSituations(body, callback)</td>
    <td style="padding:15px">getSimilarSituations</td>
    <td style="padding:15px">{base_path}/{version}/getSimilarSituations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSituationActions(body, callback)</td>
    <td style="padding:15px">getSituationActions</td>
    <td style="padding:15px">{base_path}/{version}/getSituationActions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSituationAlertIds(body, callback)</td>
    <td style="padding:15px">getSituationAlertIds</td>
    <td style="padding:15px">{base_path}/{version}/getSituationAlertIds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSituationDescription(body, callback)</td>
    <td style="padding:15px">getSituationDescription</td>
    <td style="padding:15px">{base_path}/{version}/getSituationDescription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSituationDetails(body, callback)</td>
    <td style="padding:15px">getSituationDetails</td>
    <td style="padding:15px">{base_path}/{version}/getSituationDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSituationFlags(sitnIds, callback)</td>
    <td style="padding:15px">getSituationFlags</td>
    <td style="padding:15px">{base_path}/{version}/getSituationFlags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSituationHosts(body, callback)</td>
    <td style="padding:15px">getSituationHosts</td>
    <td style="padding:15px">{base_path}/{version}/getSituationHosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSituationIds(body, callback)</td>
    <td style="padding:15px">getSituationIds</td>
    <td style="padding:15px">{base_path}/{version}/getSituationIds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSituationPrimaryTeam(body, callback)</td>
    <td style="padding:15px">getSituationPrimaryTeam</td>
    <td style="padding:15px">{base_path}/{version}/getSituationPrimaryTeam?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSituationProcesses(body, callback)</td>
    <td style="padding:15px">getSituationProcesses</td>
    <td style="padding:15px">{base_path}/{version}/getSituationProcesses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSituationServices(body, callback)</td>
    <td style="padding:15px">getSituationServices</td>
    <td style="padding:15px">{base_path}/{version}/getSituationServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSituationsWithFlag(flag, callback)</td>
    <td style="padding:15px">getSituationsWithFlag</td>
    <td style="padding:15px">{base_path}/{version}/getSituationsWithFlag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSituationTopology(callback)</td>
    <td style="padding:15px">getSituationTopology</td>
    <td style="padding:15px">{base_path}/{version}/getSituationTopology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSituationVisualization(body, callback)</td>
    <td style="padding:15px">getSituationVisualization</td>
    <td style="padding:15px">{base_path}/{version}/getSituationVisualization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamSituationIds(body, callback)</td>
    <td style="padding:15px">getTeamSituationIds</td>
    <td style="padding:15px">{base_path}/{version}/getTeamSituationIds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mergeSituations(authToken, situations, supersedeOriginal, callback)</td>
    <td style="padding:15px">mergeSituations</td>
    <td style="padding:15px">{base_path}/{version}/mergeSituations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rateSituation(body, callback)</td>
    <td style="padding:15px">rateSituation</td>
    <td style="padding:15px">{base_path}/{version}/rateSituation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeAlertFromSituation(body, callback)</td>
    <td style="padding:15px">removeAlertFromSituation</td>
    <td style="padding:15px">{base_path}/{version}/removeAlertFromSituation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeSigCorrelationInfo(body, callback)</td>
    <td style="padding:15px">removeSigCorrelationInfo</td>
    <td style="padding:15px">{base_path}/{version}/removeSigCorrelationInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeSituationPrimaryTeam(body, callback)</td>
    <td style="padding:15px">removeSituationPrimaryTeam</td>
    <td style="padding:15px">{base_path}/{version}/removeSituationPrimaryTeam?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resolveSituation(body, callback)</td>
    <td style="padding:15px">resolveSituation</td>
    <td style="padding:15px">{base_path}/{version}/resolveSituation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSituationAcknowledgeState(body, callback)</td>
    <td style="padding:15px">setSituationAcknowledgeState</td>
    <td style="padding:15px">{base_path}/{version}/setSituationAcknowledgeState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSituationDescription(body, callback)</td>
    <td style="padding:15px">setSituationDescription</td>
    <td style="padding:15px">{base_path}/{version}/setSituationDescription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSituationFlags(body, callback)</td>
    <td style="padding:15px">setSituationFlags</td>
    <td style="padding:15px">{base_path}/{version}/setSituationFlags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSituationPrimaryTeam(body, callback)</td>
    <td style="padding:15px">setSituationPrimaryTeam</td>
    <td style="padding:15px">{base_path}/{version}/setSituationPrimaryTeam?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSituationProcesses(body, callback)</td>
    <td style="padding:15px">setSituationProcesses</td>
    <td style="padding:15px">{base_path}/{version}/setSituationProcesses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSituationServices(body, callback)</td>
    <td style="padding:15px">setSituationServices</td>
    <td style="padding:15px">{base_path}/{version}/setSituationServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAcknowledgedSituationsPerUserStats(body, callback)</td>
    <td style="padding:15px">getAcknowledgedSituationsPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getAcknowledgedSituationsPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssignedSituationsPerUserStats(body, callback)</td>
    <td style="padding:15px">getAssignedSituationsPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getAssignedSituationsPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClosedSituationsPerUserStats(body, callback)</td>
    <td style="padding:15px">getClosedSituationsPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getClosedSituationsPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMTTAStats(body, callback)</td>
    <td style="padding:15px">getMTTAStats</td>
    <td style="padding:15px">{base_path}/{version}/getMTTAStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMTTAPerTeamStats(body, callback)</td>
    <td style="padding:15px">getMTTAPerTeamStats</td>
    <td style="padding:15px">{base_path}/{version}/getMTTAPerTeamStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMTTAPerUserStats(body, callback)</td>
    <td style="padding:15px">getMTTAPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getMTTAPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMTTDStats(body, callback)</td>
    <td style="padding:15px">getMTTDStats</td>
    <td style="padding:15px">{base_path}/{version}/getMTTDStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMTTRStats(body, callback)</td>
    <td style="padding:15px">getMTTRStats</td>
    <td style="padding:15px">{base_path}/{version}/getMTTRStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMTTRPerTeamStats(body, callback)</td>
    <td style="padding:15px">getMTTRPerTeamStats</td>
    <td style="padding:15px">{base_path}/{version}/getMTTRPerTeamStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMTTRPerUserStats(body, callback)</td>
    <td style="padding:15px">getMTTRPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getMTTRPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNewAlertsPerSituationsStats(body, callback)</td>
    <td style="padding:15px">getNewAlertsPerSituationsStats</td>
    <td style="padding:15px">{base_path}/{version}/getNewAlertsPerSituationsStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNewEventsPerSituationsStats(body, callback)</td>
    <td style="padding:15px">getNewEventsPerSituationsStats</td>
    <td style="padding:15px">{base_path}/{version}/getNewEventsPerSituationsStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNewSituationsStats(body, callback)</td>
    <td style="padding:15px">getNewSituationsStats</td>
    <td style="padding:15px">{base_path}/{version}/getNewSituationsStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpenSituationsPerUserStats(body, callback)</td>
    <td style="padding:15px">getOpenSituationsPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getOpenSituationsPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRatedSituationsPerUserStats(body, callback)</td>
    <td style="padding:15px">getRatedSituationsPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getRatedSituationsPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReassignedSituationsPerTeamStats(body, callback)</td>
    <td style="padding:15px">getReassignedSituationsPerTeamStats</td>
    <td style="padding:15px">{base_path}/{version}/getReassignedSituationsPerTeamStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReassignedSituationsPerUserStats(body, callback)</td>
    <td style="padding:15px">getReassignedSituationsPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getReassignedSituationsPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReassignedSituationStats(body, callback)</td>
    <td style="padding:15px">getReassignedSituationStats</td>
    <td style="padding:15px">{base_path}/{version}/getReassignedSituationStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReoccurringSituationPerTeamStats(body, callback)</td>
    <td style="padding:15px">getReoccurringSituationPerTeamStats</td>
    <td style="padding:15px">{base_path}/{version}/getReoccurringSituationPerTeamStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReoccurringSituationStats(body, callback)</td>
    <td style="padding:15px">getReoccurringSituationStats</td>
    <td style="padding:15px">{base_path}/{version}/getReoccurringSituationStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResolvedSituationsPerUserStats(body, callback)</td>
    <td style="padding:15px">getResolvedSituationsPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getResolvedSituationsPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceSituationPerTeamStats(body, callback)</td>
    <td style="padding:15px">getServiceSituationPerTeamStats</td>
    <td style="padding:15px">{base_path}/{version}/getServiceSituationPerTeamStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceSituationStats(body, callback)</td>
    <td style="padding:15px">getServiceSituationStats</td>
    <td style="padding:15px">{base_path}/{version}/getServiceSituationStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSeveritySituationPerTeamStats(body, callback)</td>
    <td style="padding:15px">getSeveritySituationPerTeamStats</td>
    <td style="padding:15px">{base_path}/{version}/getSeveritySituationPerTeamStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSeveritySituationStats(body, callback)</td>
    <td style="padding:15px">getSeveritySituationStats</td>
    <td style="padding:15px">{base_path}/{version}/getSeveritySituationStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusSituationPerTeamStats(body, callback)</td>
    <td style="padding:15px">getStatusSituationPerTeamStats</td>
    <td style="padding:15px">{base_path}/{version}/getStatusSituationPerTeamStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusSituationStats(body, callback)</td>
    <td style="padding:15px">getStatusSituationStats</td>
    <td style="padding:15px">{base_path}/{version}/getStatusSituationStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemSituationStats(body, callback)</td>
    <td style="padding:15px">getSystemSituationStats</td>
    <td style="padding:15px">{base_path}/{version}/getSystemSituationStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamSituationStats(body, callback)</td>
    <td style="padding:15px">getTeamSituationStats</td>
    <td style="padding:15px">{base_path}/{version}/getTeamSituationStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getViewedSituationsPerUserStats(body, callback)</td>
    <td style="padding:15px">getViewedSituationsPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getViewedSituationsPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkedSituationsPerUserStats(body, callback)</td>
    <td style="padding:15px">getWorkedSituationsPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getWorkedSituationsPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCookbook(body, callback)</td>
    <td style="padding:15px">addCookbook</td>
    <td style="padding:15px">{base_path}/{version}/addCookbook?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCookbook(body, callback)</td>
    <td style="padding:15px">deleteCookbook</td>
    <td style="padding:15px">{base_path}/{version}/deleteCookbook?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCookbooks(callback)</td>
    <td style="padding:15px">getCookbooks</td>
    <td style="padding:15px">{base_path}/{version}/getCookbooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCookbook(body, callback)</td>
    <td style="padding:15px">updateCookbook</td>
    <td style="padding:15px">{base_path}/{version}/updateCookbook?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBotRecipe(body, callback)</td>
    <td style="padding:15px">addBotRecipe</td>
    <td style="padding:15px">{base_path}/{version}/addBotRecipe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addValueRecipe(body, callback)</td>
    <td style="padding:15px">addValueRecipe</td>
    <td style="padding:15px">{base_path}/{version}/addValueRecipe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRecipe(body, callback)</td>
    <td style="padding:15px">deleteRecipe</td>
    <td style="padding:15px">{base_path}/{version}/deleteRecipe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRecipes(callback)</td>
    <td style="padding:15px">getRecipes</td>
    <td style="padding:15px">{base_path}/{version}/getRecipes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBotRecipe(body, callback)</td>
    <td style="padding:15px">updateBotRecipe</td>
    <td style="padding:15px">{base_path}/{version}/updateBotRecipe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateValueRecipe(body, callback)</td>
    <td style="padding:15px">updateValueRecipe</td>
    <td style="padding:15px">{base_path}/{version}/updateValueRecipe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addMergeGroup(body, callback)</td>
    <td style="padding:15px">addMergeGroup</td>
    <td style="padding:15px">{base_path}/{version}/addMergeGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMergeGroup(body, callback)</td>
    <td style="padding:15px">deleteMergeGroup</td>
    <td style="padding:15px">{base_path}/{version}/deleteMergeGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefaultMergeGroup(callback)</td>
    <td style="padding:15px">getDefaultMergeGroup</td>
    <td style="padding:15px">{base_path}/{version}/getDefaultMergeGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMergeGroups(callback)</td>
    <td style="padding:15px">getMergeGroups</td>
    <td style="padding:15px">{base_path}/{version}/getMergeGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDefaultMergeGroup(body, callback)</td>
    <td style="padding:15px">updateDefaultMergeGroup</td>
    <td style="padding:15px">{base_path}/{version}/updateDefaultMergeGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMergeGroup(body, callback)</td>
    <td style="padding:15px">updateMergeGroup</td>
    <td style="padding:15px">{base_path}/{version}/updateMergeGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addProcess(body, callback)</td>
    <td style="padding:15px">addProcess</td>
    <td style="padding:15px">{base_path}/{version}/addProcess?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProcesses(body, callback)</td>
    <td style="padding:15px">getProcesses</td>
    <td style="padding:15px">{base_path}/{version}/getProcesses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addService(body, callback)</td>
    <td style="padding:15px">addService</td>
    <td style="padding:15px">{base_path}/{version}/addService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServices(body, callback)</td>
    <td style="padding:15px">getServices</td>
    <td style="padding:15px">{base_path}/{version}/getServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsForService(serviceId, serviceName, callback)</td>
    <td style="padding:15px">getTeamsForService</td>
    <td style="padding:15px">{base_path}/{version}/getTeamsForService?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTempus(body, callback)</td>
    <td style="padding:15px">addTempus</td>
    <td style="padding:15px">{base_path}/{version}/addTempus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTempus(body, callback)</td>
    <td style="padding:15px">deleteTempus</td>
    <td style="padding:15px">{base_path}/{version}/deleteTempus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTempus(callback)</td>
    <td style="padding:15px">getTempus</td>
    <td style="padding:15px">{base_path}/{version}/getTempus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTempus(body, callback)</td>
    <td style="padding:15px">updateTempus</td>
    <td style="padding:15px">{base_path}/{version}/updateTempus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyNewLicense(body, callback)</td>
    <td style="padding:15px">applyNewLicense</td>
    <td style="padding:15px">{base_path}/{version}/applyNewLicence?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMaintenanceWindow(body, callback)</td>
    <td style="padding:15px">createMaintenanceWindow</td>
    <td style="padding:15px">{base_path}/{version}/createMaintenanceWindow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMaintenanceWindow(body, callback)</td>
    <td style="padding:15px">deleteMaintenanceWindow</td>
    <td style="padding:15px">{base_path}/{version}/deleteMaintenanceWindow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMaintenanceWindows(body, callback)</td>
    <td style="padding:15px">deleteMaintenanceWindows</td>
    <td style="padding:15px">{base_path}/{version}/deleteMaintenanceWindows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findMaintenanceWindows(body, callback)</td>
    <td style="padding:15px">findMaintenanceWindows</td>
    <td style="padding:15px">{base_path}/{version}/findMaintenanceWindows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaintenanceWindows(body, callback)</td>
    <td style="padding:15px">getMaintenanceWindows</td>
    <td style="padding:15px">{base_path}/{version}/getMaintenanceWindows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMaintenanceWindow(body, callback)</td>
    <td style="padding:15px">updateMaintenanceWindow</td>
    <td style="padding:15px">{base_path}/{version}/updateMaintenanceWindow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSecurityRealm(body, callback)</td>
    <td style="padding:15px">createSecurityRealm</td>
    <td style="padding:15px">{base_path}/{version}/createSecurityRealm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityRealm(callback)</td>
    <td style="padding:15px">getSecurityRealm</td>
    <td style="padding:15px">{base_path}/{version}/getSecurityRealms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSecurityRealm(body, callback)</td>
    <td style="padding:15px">updateSecurityRealm</td>
    <td style="padding:15px">{base_path}/{version}/updateSecurityRealm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTeam(body, callback)</td>
    <td style="padding:15px">createTeam</td>
    <td style="padding:15px">{base_path}/{version}/createTeam?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeam(body, callback)</td>
    <td style="padding:15px">deleteTeam</td>
    <td style="padding:15px">{base_path}/{version}/deleteTeam?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeam(teamId, callback)</td>
    <td style="padding:15px">getTeam</td>
    <td style="padding:15px">{base_path}/{version}/getTeam?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeams(callback)</td>
    <td style="padding:15px">getTeams</td>
    <td style="padding:15px">{base_path}/{version}/getTeams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTeam(body, callback)</td>
    <td style="padding:15px">updateTeam</td>
    <td style="padding:15px">{base_path}/{version}/updateTeam?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommentCountPerTeamStats(body, callback)</td>
    <td style="padding:15px">getCommentCountPerTeamStats</td>
    <td style="padding:15px">{base_path}/{version}/getCommentCountPerTeamStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addThreadEntry(body, callback)</td>
    <td style="padding:15px">addThreadEntry</td>
    <td style="padding:15px">{base_path}/{version}/addThreadEntry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createThread(body, callback)</td>
    <td style="padding:15px">createThread</td>
    <td style="padding:15px">{base_path}/{version}/createThread?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createThreadEntry(body, callback)</td>
    <td style="padding:15px">createThreadEntry</td>
    <td style="padding:15px">{base_path}/{version}/createThreadEntry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResolvingThreadEntries(body, callback)</td>
    <td style="padding:15px">getResolvingThreadEntries</td>
    <td style="padding:15px">{base_path}/{version}/getResolvingThreadEntries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThreadEntries(body, callback)</td>
    <td style="padding:15px">getThreadEntries</td>
    <td style="padding:15px">{base_path}/{version}/getThreadEntries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setResolvingThreadEntry(body, callback)</td>
    <td style="padding:15px">setResolvingThreadEntry</td>
    <td style="padding:15px">{base_path}/{version}/setResolvingThreadEntry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, callback)</td>
    <td style="padding:15px">createUser</td>
    <td style="padding:15px">{base_path}/{version}/createUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserInfo(body, callback)</td>
    <td style="padding:15px">getUserInfo</td>
    <td style="padding:15px">{base_path}/{version}/getUserInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserRoles(body, callback)</td>
    <td style="padding:15px">getUserRoles</td>
    <td style="padding:15px">{base_path}/{version}/getUserRoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(body, callback)</td>
    <td style="padding:15px">getUsers</td>
    <td style="padding:15px">{base_path}/{version}/getUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserTeams(body, callback)</td>
    <td style="padding:15px">getUserTeams</td>
    <td style="padding:15px">{base_path}/{version}/getUserTeams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUser(body, callback)</td>
    <td style="padding:15px">updateUser</td>
    <td style="padding:15px">{base_path}/{version}/updateUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommentCountPerUserStats(body, callback)</td>
    <td style="padding:15px">getCommentCountPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getCommentCountPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInvitationsReceivedPerUserStats(body, callback)</td>
    <td style="padding:15px">getInvitationsReceivedPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getInvitationsReceivedPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWorkflow(body, callback)</td>
    <td style="padding:15px">createWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/createWorkflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkflow(body, callback)</td>
    <td style="padding:15px">deleteWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/deleteWorkflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowEngineMoolets(callback)</td>
    <td style="padding:15px">getWorkflowEngineMoolets</td>
    <td style="padding:15px">{base_path}/{version}/getWorkflowEngineMoolets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflows(body, callback)</td>
    <td style="padding:15px">getWorkflows</td>
    <td style="padding:15px">{base_path}/{version}/getWorkflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reorderWorkflows(body, callback)</td>
    <td style="padding:15px">reorderWorkflows</td>
    <td style="padding:15px">{base_path}/{version}/reorderWorkflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWorkflow(body, callback)</td>
    <td style="padding:15px">updateWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/updateWorkflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGrazeIntegrationConfig(integrationId, callback)</td>
    <td style="padding:15px">getGrazeIntegrationConfig</td>
    <td style="padding:15px">{base_path}/{version}/getIntegrationConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ons(body, callback)</td>
    <td style="padding:15px">ons</td>
    <td style="padding:15px">{base_path}/{version}/integrations/api/v1/integrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrationConfig(integrationId, callback)</td>
    <td style="padding:15px">getIntegrationConfig</td>
    <td style="padding:15px">{base_path}/{version}/integrations/api/v1/integrations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIntegrationConfig(body, integrationId, callback)</td>
    <td style="padding:15px">updateIntegrationConfig</td>
    <td style="padding:15px">{base_path}/{version}/integrations/api/v1/integrations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrationStatus(integrationId, callback)</td>
    <td style="padding:15px">getIntegrationStatus</td>
    <td style="padding:15px">{base_path}/{version}/integrations/api/v1/integrations/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIntegrationStatus(body, integrationId, callback)</td>
    <td style="padding:15px">updateIntegrationStatus</td>
    <td style="padding:15px">{base_path}/{version}/integrations/api/v1/integrations/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getToolShares(body, callback)</td>
    <td style="padding:15px">getToolShares</td>
    <td style="padding:15px">{base_path}/{version}/getToolShares?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">shareToolAccess(body, callback)</td>
    <td style="padding:15px">shareToolAccess</td>
    <td style="padding:15px">{base_path}/{version}/shareToolAccess?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChatOpsToolExecutedPerUserStats(body, callback)</td>
    <td style="padding:15px">getChatOpsToolExecutedPerUserStats</td>
    <td style="padding:15px">{base_path}/{version}/getChatOpsToolExecutedPerUserStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemStatus(callback)</td>
    <td style="padding:15px">getSystemStatus</td>
    <td style="padding:15px">{base_path}/{version}/getSystemStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemSummary(callback)</td>
    <td style="padding:15px">getSystemSummary</td>
    <td style="padding:15px">{base_path}/{version}/getSystemSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStats(callback)</td>
    <td style="padding:15px">getStats</td>
    <td style="padding:15px">{base_path}/{version}/getStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authentication(username, password, callback)</td>
    <td style="padding:15px">Authentication</td>
    <td style="padding:15px">{base_path}/{version}/authenticate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
