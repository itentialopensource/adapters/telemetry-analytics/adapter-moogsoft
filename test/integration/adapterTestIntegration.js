/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-moogsoft',
      type: 'Moogsoft',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Moogsoft = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Moogsoft Adapter Test', () => {
  describe('Moogsoft Class Tests', () => {
    const a = new Moogsoft(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const alertsAddAlertCustomInfoBodyParam = {
      alert_id: 1,
      custom_info: {}
    };
    describe('#addAlertCustomInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAlertCustomInfo(alertsAddAlertCustomInfoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'addAlertCustomInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsAssignAlertBodyParam = {
      alert_id: 7,
      username: samProps.authentication.username
    };
    describe('#assignAlert - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignAlert(alertsAssignAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'assignAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsAssignAndAcknowledgeAlertBodyParam = {};
    describe('#assignAndAcknowledgeAlert - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignAndAcknowledgeAlert(alertsAssignAndAcknowledgeAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'assignAndAcknowledgeAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsCloseAlertBodyParam = {
      alert_ids: [
        {}
      ],
      thread_entry_comment: 'string'
    };
    describe('#closeAlert - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.closeAlert(alertsCloseAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'closeAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsDeassignAlertBodyParam = {};
    describe('#deassignAlert - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deassignAlert(alertsDeassignAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'deassignAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsResolveAlertsBodyParam = {
      alert_ids: [
        {}
      ],
      thread_entry_comment: 'string'
    };
    describe('#resolveAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.resolveAlerts(alertsResolveAlertsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.status);
                assert.equal(true, Array.isArray(data.response.resolved_alerts));
                assert.equal(true, Array.isArray(data.response.failed_alerts));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'resolveAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsSetAlertAcknowledgeStateBodyParam = {
      alert_id: 9,
      acknowledged: 5
    };
    describe('#setAlertAcknowledgeState - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setAlertAcknowledgeState(alertsSetAlertAcknowledgeStateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'setAlertAcknowledgeState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsSetAlertSeverityBodyParam = {
      alert_id: 7,
      severity: 8
    };
    describe('#setAlertSeverity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setAlertSeverity(alertsSetAlertSeverityBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'setAlertSeverity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsSetPrcLabelsBodyParam = {
      sitn_id: 'string',
      causal: 'string',
      non_causal: 'string',
      unlabelled: 'string'
    };
    describe('#setPrcLabels - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setPrcLabels(alertsSetPrcLabelsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'setPrcLabels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsGetAlertActionsBodyParam = {};
    describe('#getAlertActions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertActions(alertsGetAlertActionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getAlertActions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsGetAlertDetailsBodyParam = {
      alert_id: 'string'
    };
    describe('#getAlertDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertDetails(alertsGetAlertDetailsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.active_sitn_list));
                assert.equal('string', data.response.agent);
                assert.equal('string', data.response.agent_location);
                assert.equal(5, data.response.alert_id);
                assert.equal('string', data.response.class);
                assert.equal(3, data.response.count);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.external_id);
                assert.equal(2, data.response.first_event_time);
                assert.equal(6, data.response.int_last_event_time);
                assert.equal(9, data.response.last_event_time);
                assert.equal(2, data.response.last_state_change);
                assert.equal('string', data.response.manager);
                assert.equal(1, data.response.owner);
                assert.equal(5, data.response.severity);
                assert.equal('string', data.response.signature);
                assert.equal(1, data.response.significance);
                assert.equal('string', data.response.source);
                assert.equal('string', data.response.source_id);
                assert.equal(8, data.response.state);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getAlertDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsGetAlertIdsBodyParam = {};
    describe('#getAlertIds - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAlertIds(alertsGetAlertIdsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getAlertIds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsGetAlertsInNewSituationsStatsBodyParam = {};
    describe('#getAlertsInNewSituationsStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertsInNewSituationsStats(alertsGetAlertsInNewSituationsStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getAlertsInNewSituationsStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsGetAlertsMarkedPRCPerUserStatsBodyParam = {};
    describe('#getAlertsMarkedPRCPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertsMarkedPRCPerUserStats(alertsGetAlertsMarkedPRCPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getAlertsMarkedPRCPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsGetNewAlertsStatsBodyParam = {};
    describe('#getNewAlertsStats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNewAlertsStats(alertsGetNewAlertsStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getNewAlertsStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsGetNewEventsPerAlertsStatsBodyParam = {};
    describe('#getNewEventsPerAlertsStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNewEventsPerAlertsStats(alertsGetNewEventsPerAlertsStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getNewEventsPerAlertsStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrcLabels - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPrcLabels(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getPrcLabels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSeverities - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSeverities((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getSeverities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatuses - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getStatuses((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getStatuses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsGetTopPrcDetailsBodyParam = {};
    describe('#getTopPrcDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopPrcDetails(alertsGetTopPrcDetailsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getTopPrcDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsAddAlertToSituationBodyParam = {
      alert_id: 8,
      sitn_id: 1
    };
    describe('#addAlertToSituation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAlertToSituation(situationsAddAlertToSituationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'addAlertToSituation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsAddSigCorrelationInfoBodyParam = {
      sitn_id: 6,
      service_name: 'string',
      resource_id: 'string'
    };
    describe('#addSigCorrelationInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSigCorrelationInfo(situationsAddSigCorrelationInfoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'addSigCorrelationInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsAddSituationCustomInfoBodyParam = {
      sitn_id: 3,
      custom_info: {}
    };
    describe('#addSituationCustomInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSituationCustomInfo(situationsAddSituationCustomInfoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'addSituationCustomInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsAssignAndAcknowledgeSituationBodyParam = {
      sitn_id: 2,
      username: samProps.authentication.username
    };
    describe('#assignAndAcknowledgeSituation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignAndAcknowledgeSituation(situationsAssignAndAcknowledgeSituationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'assignAndAcknowledgeSituation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsAssignSituationBodyParam = {
      sitn_id: 6,
      user_id: 1
    };
    describe('#assignSituation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignSituation(situationsAssignSituationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'assignSituation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsAssignTeamsToSituationBodyParam = {
      sitn_id: 4,
      team_ids: [
        {}
      ]
    };
    describe('#assignTeamsToSituation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.assignTeamsToSituation(situationsAssignTeamsToSituationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.team_ids));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'assignTeamsToSituation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsCloseSituationBodyParam = {
      sitn_id: 8,
      resolution: 3
    };
    describe('#closeSituation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.closeSituation(situationsCloseSituationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'closeSituation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsCreateSituationBodyParam = {
      description: 'string'
    };
    describe('#createSituation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSituation(situationsCreateSituationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.sitnId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'createSituation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsDeassignSituationBodyParam = {
      sitn_id: 9
    };
    describe('#deassignSituation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deassignSituation(situationsDeassignSituationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'deassignSituation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mergeSituations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.mergeSituations(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.sitnId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'mergeSituations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsRateSituationBodyParam = {
      sig_id: 9,
      rating: 'string',
      comment: 'string'
    };
    describe('#rateSituation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rateSituation(situationsRateSituationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.rating);
                assert.equal('string', data.response.comment);
                assert.equal(2, data.response.sitnId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'rateSituation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsRemoveAlertFromSituationBodyParam = {
      alert_id: 3,
      sitn_id: 1
    };
    describe('#removeAlertFromSituation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeAlertFromSituation(situationsRemoveAlertFromSituationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'removeAlertFromSituation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsRemoveSigCorrelationInfoBodyParam = {
      sitn_id: 6,
      service_name: 'string',
      external_id: 'string'
    };
    describe('#removeSigCorrelationInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeSigCorrelationInfo(situationsRemoveSigCorrelationInfoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'removeSigCorrelationInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsResolveSituationBodyParam = {
      sitn_id: 8
    };
    describe('#resolveSituation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resolveSituation(situationsResolveSituationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'resolveSituation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsSetSituationAcknowledgeStateBodyParam = {
      sitn_id: 2,
      acknowledged: 9
    };
    describe('#setSituationAcknowledgeState - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setSituationAcknowledgeState(situationsSetSituationAcknowledgeStateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'setSituationAcknowledgeState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsSetSituationDescriptionBodyParam = {
      sitn_id: 2,
      description: 'string'
    };
    describe('#setSituationDescription - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setSituationDescription(situationsSetSituationDescriptionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'setSituationDescription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsSetSituationFlagsBodyParam = {
      sitn_ids: [
        {}
      ],
      to_add: [
        {}
      ],
      to_remove: [
        {}
      ]
    };
    describe('#setSituationFlags - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setSituationFlags(situationsSetSituationFlagsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'setSituationFlags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsSetSituationPrimaryTeamBodyParam = {
      sitn_id: 3,
      team_name: 'string'
    };
    describe('#setSituationPrimaryTeam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.setSituationPrimaryTeam(situationsSetSituationPrimaryTeamBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.sitnId);
                assert.equal(10, data.response.primary_team_id);
                assert.equal('string', data.response.primary_team_name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'setSituationPrimaryTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsSetSituationProcessesBodyParam = {
      sitn_id: 2,
      process_list: [
        {}
      ]
    };
    describe('#setSituationProcesses - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setSituationProcesses(situationsSetSituationProcessesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'setSituationProcesses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsSetSituationServicesBodyParam = {
      sitn_id: 5,
      service_list: [
        {}
      ]
    };
    describe('#setSituationServices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setSituationServices(situationsSetSituationServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'setSituationServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkSituationFlag - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.checkSituationFlag(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'checkSituationFlag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetAcknowledgedSituationsPerUserStatsBodyParam = {};
    describe('#getAcknowledgedSituationsPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAcknowledgedSituationsPerUserStats(situationsGetAcknowledgedSituationsPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getAcknowledgedSituationsPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveSituationIds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getActiveSituationIds((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total_situations);
                assert.equal(true, Array.isArray(data.response.sitnIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getActiveSituationIds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetAssignedSituationsPerUserStatsBodyParam = {};
    describe('#getAssignedSituationsPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAssignedSituationsPerUserStats(situationsGetAssignedSituationsPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getAssignedSituationsPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetClosedSituationsPerUserStatsBodyParam = {};
    describe('#getClosedSituationsPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getClosedSituationsPerUserStats(situationsGetClosedSituationsPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getClosedSituationsPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetMTTAPerTeamStatsBodyParam = {};
    describe('#getMTTAPerTeamStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMTTAPerTeamStats(situationsGetMTTAPerTeamStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getMTTAPerTeamStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetMTTAPerUserStatsBodyParam = {};
    describe('#getMTTAPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMTTAPerUserStats(situationsGetMTTAPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getMTTAPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetMTTAStatsBodyParam = {};
    describe('#getMTTAStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMTTAStats(situationsGetMTTAStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getMTTAStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetMTTDStatsBodyParam = {};
    describe('#getMTTDStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMTTDStats(situationsGetMTTDStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getMTTDStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetMTTRPerTeamStatsBodyParam = {};
    describe('#getMTTRPerTeamStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMTTRPerTeamStats(situationsGetMTTRPerTeamStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getMTTRPerTeamStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetMTTRPerUserStatsBodyParam = {};
    describe('#getMTTRPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMTTRPerUserStats(situationsGetMTTRPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getMTTRPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetMTTRStatsBodyParam = {};
    describe('#getMTTRStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMTTRStats(situationsGetMTTRStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getMTTRStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetNewAlertsPerSituationsStatsBodyParam = {};
    describe('#getNewAlertsPerSituationsStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNewAlertsPerSituationsStats(situationsGetNewAlertsPerSituationsStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getNewAlertsPerSituationsStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetNewEventsPerSituationsStatsBodyParam = {};
    describe('#getNewEventsPerSituationsStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNewEventsPerSituationsStats(situationsGetNewEventsPerSituationsStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getNewEventsPerSituationsStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetNewSituationsStatsBodyParam = {};
    describe('#getNewSituationsStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNewSituationsStats(situationsGetNewSituationsStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getNewSituationsStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetOpenSituationsPerUserStatsBodyParam = {};
    describe('#getOpenSituationsPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpenSituationsPerUserStats(situationsGetOpenSituationsPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getOpenSituationsPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetRatedSituationsPerUserStatsBodyParam = {};
    describe('#getRatedSituationsPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRatedSituationsPerUserStats(situationsGetRatedSituationsPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getRatedSituationsPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetReassignedSituationStatsBodyParam = {};
    describe('#getReassignedSituationStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReassignedSituationStats(situationsGetReassignedSituationStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getReassignedSituationStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetReassignedSituationsPerTeamStatsBodyParam = {};
    describe('#getReassignedSituationsPerTeamStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReassignedSituationsPerTeamStats(situationsGetReassignedSituationsPerTeamStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getReassignedSituationsPerTeamStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetReassignedSituationsPerUserStatsBodyParam = {};
    describe('#getReassignedSituationsPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReassignedSituationsPerUserStats(situationsGetReassignedSituationsPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getReassignedSituationsPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetReoccurringSituationPerTeamStatsBodyParam = {};
    describe('#getReoccurringSituationPerTeamStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReoccurringSituationPerTeamStats(situationsGetReoccurringSituationPerTeamStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getReoccurringSituationPerTeamStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetReoccurringSituationStatsBodyParam = {};
    describe('#getReoccurringSituationStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReoccurringSituationStats(situationsGetReoccurringSituationStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getReoccurringSituationStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetResolvedSituationsPerUserStatsBodyParam = {};
    describe('#getResolvedSituationsPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getResolvedSituationsPerUserStats(situationsGetResolvedSituationsPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getResolvedSituationsPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetServiceSituationPerTeamStatsBodyParam = {};
    describe('#getServiceSituationPerTeamStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServiceSituationPerTeamStats(situationsGetServiceSituationPerTeamStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getServiceSituationPerTeamStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetServiceSituationStatsBodyParam = {};
    describe('#getServiceSituationStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServiceSituationStats(situationsGetServiceSituationStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getServiceSituationStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSeveritySituationPerTeamStatsBodyParam = {};
    describe('#getSeveritySituationPerTeamStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSeveritySituationPerTeamStats(situationsGetSeveritySituationPerTeamStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSeveritySituationPerTeamStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSeveritySituationStatsBodyParam = {};
    describe('#getSeveritySituationStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSeveritySituationStats(situationsGetSeveritySituationStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSeveritySituationStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSigCorrelationInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSigCorrelationInfo(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSigCorrelationInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSimilarSituationIdsBodyParam = {};
    describe('#getSimilarSituationIds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSimilarSituationIds(situationsGetSimilarSituationIdsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.similarity_ids));
                assert.equal(10, data.response.sig_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSimilarSituationIds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSimilarSituationsBodyParam = {};
    describe('#getSimilarSituations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSimilarSituations(situationsGetSimilarSituationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.similarities));
                assert.equal(5, data.response.similar_count);
                assert.equal(7, data.response.sig_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSimilarSituations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSituationActionsBodyParam = {};
    describe('#getSituationActions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSituationActions(situationsGetSituationActionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.activities));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSituationActions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSituationAlertIdsBodyParam = {
      sitn_id: 'string',
      for_unique_alerts: 'string'
    };
    describe('#getSituationAlertIds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSituationAlertIds(situationsGetSituationAlertIdsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.total_alerts);
                assert.equal(true, Array.isArray(data.response.alert_ids));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSituationAlertIds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSituationDescriptionBodyParam = {};
    describe('#getSituationDescription - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSituationDescription(situationsGetSituationDescriptionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.sitnId);
                assert.equal('string', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSituationDescription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSituationDetailsBodyParam = {};
    describe('#getSituationDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSituationDetails(situationsGetSituationDetailsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.category);
                assert.equal(3, data.response.created_at);
                assert.equal('string', data.response.description);
                assert.equal(7, data.response.first_event_time);
                assert.equal(9, data.response.internal_priority);
                assert.equal(4, data.response.last_event_time);
                assert.equal(10, data.response.last_state_change);
                assert.equal(3, data.response.moderator_id);
                assert.equal(3, data.response.sitnId);
                assert.equal(9, data.response.status);
                assert.equal(5, data.response.story_id);
                assert.equal(1, data.response.total_alerts);
                assert.equal(4, data.response.total_unique_alerts);
                assert.equal(2, data.response.primary_team_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSituationDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSituationFlags - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSituationFlags(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSituationFlags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSituationHostsBodyParam = {};
    describe('#getSituationHosts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSituationHosts(situationsGetSituationHostsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSituationHosts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSituationIdsBodyParam = {};
    describe('#getSituationIds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSituationIds(situationsGetSituationIdsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.total_situations);
                assert.equal(true, Array.isArray(data.response.sitnIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSituationIds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSituationPrimaryTeamBodyParam = {};
    describe('#getSituationPrimaryTeam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSituationPrimaryTeam(situationsGetSituationPrimaryTeamBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.primary_team_name);
                assert.equal(10, data.response.sitnId);
                assert.equal(5, data.response.primary_team_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSituationPrimaryTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSituationProcessesBodyParam = {};
    describe('#getSituationProcesses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSituationProcesses(situationsGetSituationProcessesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.processes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSituationProcesses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSituationServicesBodyParam = {};
    describe('#getSituationServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSituationServices(situationsGetSituationServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.services));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSituationServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSituationTopology - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSituationTopology((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.links));
                assert.equal(true, Array.isArray(data.response.nodes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSituationTopology', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSituationVisualizationBodyParam = {
      sitn_id: 'string'
    };
    describe('#getSituationVisualization - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSituationVisualization(situationsGetSituationVisualizationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSituationVisualization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSituationsWithFlag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSituationsWithFlag(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSituationsWithFlag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetStatusSituationPerTeamStatsBodyParam = {};
    describe('#getStatusSituationPerTeamStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatusSituationPerTeamStats(situationsGetStatusSituationPerTeamStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getStatusSituationPerTeamStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetStatusSituationStatsBodyParam = {};
    describe('#getStatusSituationStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatusSituationStats(situationsGetStatusSituationStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getStatusSituationStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetSystemSituationStatsBodyParam = {};
    describe('#getSystemSituationStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemSituationStats(situationsGetSystemSituationStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getSystemSituationStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetTeamSituationIdsBodyParam = {
      team_name: 'string'
    };
    describe('#getTeamSituationIds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamSituationIds(situationsGetTeamSituationIdsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total_situations);
                assert.equal(true, Array.isArray(data.response.sitnIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getTeamSituationIds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetTeamSituationStatsBodyParam = {};
    describe('#getTeamSituationStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamSituationStats(situationsGetTeamSituationStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getTeamSituationStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetViewedSituationsPerUserStatsBodyParam = {};
    describe('#getViewedSituationsPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getViewedSituationsPerUserStats(situationsGetViewedSituationsPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getViewedSituationsPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsGetWorkedSituationsPerUserStatsBodyParam = {};
    describe('#getWorkedSituationsPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkedSituationsPerUserStats(situationsGetWorkedSituationsPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'getWorkedSituationsPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const situationsRemoveSituationPrimaryTeamBodyParam = {};
    describe('#removeSituationPrimaryTeam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.removeSituationPrimaryTeam(situationsRemoveSituationPrimaryTeamBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.sitnId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Situations', 'removeSituationPrimaryTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cookbooksAddCookbookBodyParam = {
      name: 'string',
      process_output_of: [
        {}
      ],
      recipes: [
        {}
      ],
      run_on_startup: false,
      first_recipe_match_only: false
    };
    describe('#addCookbook - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addCookbook(cookbooksAddCookbookBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cookbooks', 'addCookbook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cookbooksDeleteCookbookBodyParam = {
      name: 'string'
    };
    describe('#deleteCookbook - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCookbook(cookbooksDeleteCookbookBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cookbooks', 'deleteCookbook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cookbooksUpdateCookbookBodyParam = {
      name: 'string',
      run_on_startup: false,
      cook_for_extension: 2,
      max_cook_for: 4
    };
    describe('#updateCookbook - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCookbook(cookbooksUpdateCookbookBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cookbooks', 'updateCookbook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCookbooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCookbooks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cookbooks', 'getCookbooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recipesAddBotRecipeBodyParam = {
      cookbooks: [
        {}
      ],
      name: 'string',
      alert_threshold: 9
    };
    describe('#addBotRecipe - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addBotRecipe(recipesAddBotRecipeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Recipes', 'addBotRecipe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recipesAddValueRecipeBodyParam = {
      cookbook: 'string',
      name: 'string',
      alert_threshold: 1,
      hop_limit: 1,
      components: [
        {
          name: 'string'
        }
      ]
    };
    describe('#addValueRecipe - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addValueRecipe(recipesAddValueRecipeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Recipes', 'addValueRecipe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recipesDeleteRecipeBodyParam = {
      name: 'string'
    };
    describe('#deleteRecipe - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRecipe(recipesDeleteRecipeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Recipes', 'deleteRecipe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recipesUpdateBotRecipeBodyParam = {
      name: 'string',
      alert_threshold: 5
    };
    describe('#updateBotRecipe - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateBotRecipe(recipesUpdateBotRecipeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Recipes', 'updateBotRecipe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recipesUpdateValueRecipeBodyParam = {
      name: 'string',
      hop_limit: 10,
      components: [
        {
          name: 'string'
        }
      ]
    };
    describe('#updateValueRecipe - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateValueRecipe(recipesUpdateValueRecipeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Recipes', 'updateValueRecipe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecipes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRecipes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Recipes', 'getRecipes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mergeGroupsAddMergeGroupBodyParam = {
      name: 'string',
      moolets: [
        {}
      ],
      alert_threshold: 3,
      situation_similarity_limit: 5
    };
    describe('#addMergeGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addMergeGroup(mergeGroupsAddMergeGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MergeGroups', 'addMergeGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mergeGroupsDeleteMergeGroupBodyParam = {
      name: 'string'
    };
    describe('#deleteMergeGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMergeGroup(mergeGroupsDeleteMergeGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MergeGroups', 'deleteMergeGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mergeGroupsUpdateDefaultMergeGroupBodyParam = {
      alert_threshold: 4
    };
    describe('#updateDefaultMergeGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDefaultMergeGroup(mergeGroupsUpdateDefaultMergeGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MergeGroups', 'updateDefaultMergeGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mergeGroupsUpdateMergeGroupBodyParam = {
      name: 'string',
      situation_similarity_limit: 5
    };
    describe('#updateMergeGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateMergeGroup(mergeGroupsUpdateMergeGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MergeGroups', 'updateMergeGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultMergeGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDefaultMergeGroup((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.alert_threshold);
                assert.equal(10, data.response.situation_similarity_limit);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MergeGroups', 'getDefaultMergeGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMergeGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMergeGroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MergeGroups', 'getMergeGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const processesAddProcessBodyParam = {
      name: 'string',
      description: 'string'
    };
    describe('#addProcess - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addProcess(processesAddProcessBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Processes', 'addProcess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const processesGetProcessesBodyParam = {};
    describe('#getProcesses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProcesses(processesGetProcessesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Processes', 'getProcesses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesAddServiceBodyParam = {
      name: 'string',
      description: 'string'
    };
    describe('#addService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addService(servicesAddServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'addService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesGetServicesBodyParam = {};
    describe('#getServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServices(servicesGetServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsForService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsForService(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getTeamsForService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tempusAddTempusBodyParam = {
      name: 'string',
      description: 'string',
      process_output_of: 'string',
      run_on_startup: false,
      entropy_threshold: 1,
      execution_interval: 9,
      window_size: 2,
      bucket_size: 10,
      arrival_spread: 8,
      minimum_arrival_similarity: 6,
      alert_threshold: 10,
      partition_by: 'string',
      significance_test: 'string',
      significance_threshold: 10,
      detection_algorithm: 'string'
    };
    describe('#addTempus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addTempus(tempusAddTempusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tempus', 'addTempus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tempusDeleteTempusBodyParam = {};
    describe('#deleteTempus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTempus(tempusDeleteTempusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tempus', 'deleteTempus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tempusUpdateTempusBodyParam = {};
    describe('#updateTempus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateTempus(tempusUpdateTempusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tempus', 'updateTempus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTempus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTempus((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tempus', 'getTempus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licensesApplyNewLicenseBodyParam = {};
    describe('#applyNewLicense - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applyNewLicense(licensesApplyNewLicenseBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'applyNewLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenanceWindowsCreateMaintenanceWindowBodyParam = {
      name: 'string',
      description: 'string',
      filter: 'string',
      start_date_time: 2,
      duration: 6,
      forward_alerts: true,
      recurring_period: 2,
      recurring_period_units: 3
    };
    describe('#createMaintenanceWindow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createMaintenanceWindow(maintenanceWindowsCreateMaintenanceWindowBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.window_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'createMaintenanceWindow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenanceWindowsDeleteMaintenanceWindowBodyParam = {
      id: [
        {}
      ]
    };
    describe('#deleteMaintenanceWindow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMaintenanceWindow(maintenanceWindowsDeleteMaintenanceWindowBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'deleteMaintenanceWindow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenanceWindowsDeleteMaintenanceWindowsBodyParam = {
      filter: 'string'
    };
    describe('#deleteMaintenanceWindows - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMaintenanceWindows(maintenanceWindowsDeleteMaintenanceWindowsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'deleteMaintenanceWindows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenanceWindowsUpdateMaintenanceWindowBodyParam = {
      window_id: 10,
      name: 'string',
      description: 'string',
      filter: 'string',
      start_date_time: 4,
      duration: 8,
      forward_alerts: true,
      recurring_period: 2,
      recurring_period_units: 2,
      timezone: 'string'
    };
    describe('#updateMaintenanceWindow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateMaintenanceWindow(maintenanceWindowsUpdateMaintenanceWindowBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.del_flag);
                assert.equal(false, data.response.forward_alerts);
                assert.equal(1, data.response.last_updated);
                assert.equal('string', data.response.timezone);
                assert.equal('string', data.response.description);
                assert.equal(4, data.response.recurring_period_units);
                assert.equal('string', data.response.filter);
                assert.equal(5, data.response.duration);
                assert.equal(5, data.response.recurring_period);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.updated_by);
                assert.equal('string', data.response.action);
                assert.equal(4, data.response.window_id);
                assert.equal(3, data.response.start_date_time);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'updateMaintenanceWindow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenanceWindowsFindMaintenanceWindowsBodyParam = {};
    describe('#findMaintenanceWindows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findMaintenanceWindows(maintenanceWindowsFindMaintenanceWindowsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.windows));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'findMaintenanceWindows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenanceWindowsGetMaintenanceWindowsBodyParam = {};
    describe('#getMaintenanceWindows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMaintenanceWindows(maintenanceWindowsGetMaintenanceWindowsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.windows));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'getMaintenanceWindows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityRealmsCreateSecurityRealmBodyParam = {};
    describe('#createSecurityRealm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSecurityRealm(securityRealmsCreateSecurityRealmBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityRealms', 'createSecurityRealm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityRealmsUpdateSecurityRealmBodyParam = {};
    describe('#updateSecurityRealm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSecurityRealm(securityRealmsUpdateSecurityRealmBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityRealms', 'updateSecurityRealm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityRealm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityRealm((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response['Test Saml Realm']);
                assert.equal('object', typeof data.response['Google realm']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityRealms', 'getSecurityRealm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsCreateTeamBodyParam = {
      name: 'string',
      alert_filter: 'string',
      services: [
        {}
      ],
      sig_filter: 'string',
      landing_page: {},
      active: true,
      description: 'string',
      users: [
        {}
      ]
    };
    describe('#createTeam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTeam(teamsCreateTeamBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.teamId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'createTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsDeleteTeamBodyParam = {};
    describe('#deleteTeam - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTeam(teamsDeleteTeamBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'deleteTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsUpdateTeamBodyParam = {
      team_id: 10,
      name: 'string',
      active: false,
      description: 'string',
      users: [
        {}
      ]
    };
    describe('#updateTeam - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateTeam(teamsUpdateTeamBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'updateTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsGetCommentCountPerTeamStatsBodyParam = {};
    describe('#getCommentCountPerTeamStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCommentCountPerTeamStats(teamsGetCommentCountPerTeamStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getCommentCountPerTeamStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeam(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.room_id);
                assert.equal('string', data.response.alert_filter);
                assert.equal(true, Array.isArray(data.response.user_ids));
                assert.equal('string', data.response.sig_filter);
                assert.equal('string', data.response.landing_page);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.active);
                assert.equal(9, data.response.teamId);
                assert.equal(true, Array.isArray(data.response.services));
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal(false, data.response.deleted);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.service_ids));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeams - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTeams((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeams', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const threadsAddThreadEntryBodyParam = {
      sitn_id: 4,
      thread_name: 'string',
      entry: 'string',
      resolving_step: false
    };
    describe('#addThreadEntry - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addThreadEntry(threadsAddThreadEntryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.entry_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Threads', 'addThreadEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const threadsCreateThreadBodyParam = {
      sitn_id: 8,
      thread_name: 'string'
    };
    describe('#createThread - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createThread(threadsCreateThreadBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Threads', 'createThread', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const threadsCreateThreadEntryBodyParam = {
      sitn_id: 8,
      thread_name: 'string',
      entry: 'string',
      resolving_step: true
    };
    describe('#createThreadEntry - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createThreadEntry(threadsCreateThreadEntryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Threads', 'createThreadEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const threadsSetResolvingThreadEntryBodyParam = {
      entry_id: 7,
      resolving_step: true
    };
    describe('#setResolvingThreadEntry - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setResolvingThreadEntry(threadsSetResolvingThreadEntryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Threads', 'setResolvingThreadEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const threadsGetResolvingThreadEntriesBodyParam = {
      sitn_id: 'string',
      start: 'string',
      limit: 'string'
    };
    describe('#getResolvingThreadEntries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getResolvingThreadEntries(threadsGetResolvingThreadEntriesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.sitn_id);
                assert.equal(true, Array.isArray(data.response.resolving_entries));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Threads', 'getResolvingThreadEntries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const threadsGetThreadEntriesBodyParam = {
      sitn_id: 'string',
      thread_name: 'string',
      start: 'string',
      limit: 'string'
    };
    describe('#getThreadEntries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getThreadEntries(threadsGetThreadEntriesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal(4, data.response.sitn_id);
                assert.equal('string', data.response.thread_name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Threads', 'getThreadEntries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersCreateUserBodyParam = {
      username: samProps.authentication.username,
      roles: [
        {}
      ],
      password: samProps.authentication.password,
      active: true,
      email: samProps.authentication.password,
      fullname: 'string',
      primary_group: 'string',
      department: 'string',
      joined: 4,
      timezone: 'string',
      contact_num: 'string',
      teams: [
        {}
      ]
    };
    describe('#createUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createUser(usersCreateUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.user_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'createUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUpdateUserBodyParam = {
      uid: 4,
      active: false,
      password: samProps.authentication.password,
      roles: [
        {}
      ],
      teams: [
        {}
      ],
      contact_num: 'string',
      timezone: 'string',
      fullname: 'string',
      department: 'string',
      primary_group: 'string',
      email: samProps.authentication.password
    };
    describe('#updateUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUser(usersUpdateUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'updateUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersGetCommentCountPerUserStatsBodyParam = {};
    describe('#getCommentCountPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCommentCountPerUserStats(usersGetCommentCountPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getCommentCountPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersGetInvitationsReceivedPerUserStatsBodyParam = {};
    describe('#getInvitationsReceivedPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInvitationsReceivedPerUserStats(usersGetInvitationsReceivedPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getInvitationsReceivedPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersGetUserInfoBodyParam = {
      user_id: 'string'
    };
    describe('#getUserInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserInfo(usersGetUserInfoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.full_name);
                assert.equal(8, data.response.user_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUserInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersGetUserRolesBodyParam = {
      username: samProps.authentication.username
    };
    describe('#getUserRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserRoles(usersGetUserRolesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUserRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersGetUserTeamsBodyParam = {
      username: samProps.authentication.username
    };
    describe('#getUserTeams - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserTeams(usersGetUserTeamsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUserTeams', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersGetUsersBodyParam = {
      limit: 'string'
    };
    describe('#getUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsers(usersGetUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowsCreateWorkflowBodyParam = {};
    describe('#createWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createWorkflow(workflowsCreateWorkflowBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflows', 'createWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowsDeleteWorkflowBodyParam = {
      id: 3
    };
    describe('#deleteWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWorkflow(workflowsDeleteWorkflowBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflows', 'deleteWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowsReorderWorkflowsBodyParam = {
      moolet_name: 'string',
      workflow_sequences: [
        {}
      ]
    };
    describe('#reorderWorkflows - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.reorderWorkflows(workflowsReorderWorkflowsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflows', 'reorderWorkflows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowsUpdateWorkflowBodyParam = {
      id: 4,
      active: false,
      workflow_name: 'string'
    };
    describe('#updateWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateWorkflow(workflowsUpdateWorkflowBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflows', 'updateWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowEngineMoolets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkflowEngineMoolets((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflows', 'getWorkflowEngineMoolets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowsGetWorkflowsBodyParam = {
      moolet_name: 'string'
    };
    describe('#getWorkflows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkflows(workflowsGetWorkflowsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflows', 'getWorkflows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const integrationsOnsBodyParam = {
      type_id: 'string',
      inputs: [
        {
          name: 'string'
        }
      ],
      name: 'string',
      version: 'string'
    };
    describe('#ons - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ons(integrationsOnsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type_id);
                assert.equal('string', data.response.version);
                assert.equal('object', typeof data.response.config);
                assert.equal(true, Array.isArray(data.response.inputs));
                assert.equal(true, Array.isArray(data.response.readonly));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integrations', 'ons', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const integrationsIntegrationId = 555;
    describe('#getGrazeIntegrationConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGrazeIntegrationConfig(integrationsIntegrationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integrations', 'getGrazeIntegrationConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const integrationsUpdateIntegrationConfigBodyParam = {
      type_id: 'string',
      inputs: [
        {
          name: 'string'
        }
      ],
      name: 'string',
      version: 'string'
    };
    describe('#updateIntegrationConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIntegrationConfig(integrationsUpdateIntegrationConfigBodyParam, integrationsIntegrationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integrations', 'updateIntegrationConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntegrationConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntegrationConfig(integrationsIntegrationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type_id);
                assert.equal('string', data.response.version);
                assert.equal('object', typeof data.response.config);
                assert.equal(true, Array.isArray(data.response.inputs));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integrations', 'getIntegrationConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const integrationsUpdateIntegrationStatusBodyParam = {
      command: 'string'
    };
    describe('#updateIntegrationStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateIntegrationStatus(integrationsUpdateIntegrationStatusBodyParam, integrationsIntegrationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-moogsoft-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integrations', 'updateIntegrationStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntegrationStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntegrationStatus(integrationsIntegrationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.global);
                assert.equal('object', typeof data.response.instances);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Integrations', 'getIntegrationStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const toolsGetToolSharesBodyParam = {
      tool_id: 7
    };
    describe('#getToolShares - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getToolShares(toolsGetToolSharesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.tool_id);
                assert.equal(true, Array.isArray(data.response.domain_ids));
                assert.equal('string', data.response.domain);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tools', 'getToolShares', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const toolsShareToolAccessBodyParam = {
      tool_id: 9,
      domain: 'string',
      domain_ids: [
        {}
      ]
    };
    describe('#shareToolAccess - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.shareToolAccess(toolsShareToolAccessBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.domain_ids));
                assert.equal('string', data.response.domain);
                assert.equal(7, data.response.tool_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tools', 'shareToolAccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const toolsGetChatOpsToolExecutedPerUserStatsBodyParam = {};
    describe('#getChatOpsToolExecutedPerUserStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getChatOpsToolExecutedPerUserStats(toolsGetChatOpsToolExecutedPerUserStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tools', 'getChatOpsToolExecutedPerUserStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStats((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemStatus((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.processes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemSummary((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_summary);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authentication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.authentication(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.auth_token);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
